package com.example.listresource

interface ApiCallback {
    fun onSuccess(response: String, code: Int) {}
    fun onError(response: String, code: Int) {}
    fun onFail(response: String) {}
}