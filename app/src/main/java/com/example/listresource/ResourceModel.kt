package com.example.listresource

import com.google.gson.annotations.SerializedName

class ResourceModel {
    var page = 0

    @SerializedName("per_page")
    var perPage = 0
    var total = 0

    @SerializedName("total_page")
    var totalPages = 0
    var data = arrayListOf<DataModel>()
    var ad = AdvModel()

    class DataModel {
        var id = 0
        var name = ""
        var year = 0
        var color = ""

        @SerializedName("pantone_value")
        var pantoneValue = ""
    }

    class AdvModel {
        var company = ""
        var url = ""
        var text = ""

    }

}
