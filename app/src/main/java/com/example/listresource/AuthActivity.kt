package com.example.listresource

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_add_user.*
import kotlinx.android.synthetic.main.activity_auth.*
import java.util.*

class AuthActivity : AppCompatActivity() {
    private lateinit var authTimer: Timer
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
        init()
    }

    private fun init() {
        signInButton.setOnClickListener {
            val email = emailEditText.text.toString()
            val password = passwordEditText.text.toString()
            if (email.isNotEmpty() && password.isNotEmpty()) auth(email, password)
            else showError("Please fill all the fields!")
        }

        usersTextView.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }
    }

    private fun showError(text: String) {
        messageTextView.setTextColor(Color.RED)
        messageTextView.text = "$text"
        if (messageTextView.visibility == View.GONE) messageTextView.visibility = View.VISIBLE
    }

    private fun authorize(token: String) {
        authLayout.visibility = View.GONE
        loadingLayout.visibility = View.VISIBLE
        loadingTextView.setTextColor(Color.GREEN)
        var authTime = 5
        authTimer = Timer()
        authTimer.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                runOnUiThread {
                    loadingTextView.text =
                        "Auth success!\nToken: $token\n\nYou will be redirected to Main page in $authTime sec."
                }
                if (authTime == 0) {
                    authTimer.cancel()
                    val intent = Intent(applicationContext, MainActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                } else authTime--
            }

        }, 0, 1000)
    }


    private fun auth(email: String, password: String) {
        val params = mutableMapOf<String, String>()
        params["email"] = email
        params["password"] = password
        ApiHandler.postRequest(LOGIN, params, object : ApiCallback {
            override fun onSuccess(response: String, code: Int) {
                authorize(response)
            }

            override fun onError(response: String, code: Int) {
                showError(response)
            }
        })
    }

}