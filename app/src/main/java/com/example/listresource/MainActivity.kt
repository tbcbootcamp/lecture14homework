package com.example.listresource

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.listresource.ui.login.DetailedInfoActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val itemsList = arrayListOf<ResourceModel.DataModel>()
    private val adapter = ResourcesAdapter(itemsList, this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    fun openDetailInfo(id: Int) {
        val intent = Intent(this, DetailedInfoActivity::class.java)
        intent.putExtra("itemId", id)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }

    private fun init() {

        addUserButton.setOnClickListener {
            val intent = Intent(this, AddUserActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }

        resourcesRecyclerView.layoutManager = LinearLayoutManager(this)
        resourcesRecyclerView.adapter = adapter

        ApiHandler.getRequest(UNKNOWN, object : ApiCallback {
            override fun onSuccess(response: String, code: Int) {
                val data = Gson().fromJson(response, ResourceModel::class.java)
                for (item in data.data) {
                    itemsList.add(item)
                    adapter.notifyItemInserted(itemsList.size - 1)
                }
            }

            override fun onError(response: String, code: Int) {
                Toast.makeText(this@MainActivity, response, Toast.LENGTH_LONG).show()
            }
        })
    }
}
