package com.example.listresource.ui.login

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.listresource.*
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_detailed_info.*
import kotlinx.android.synthetic.main.resource_item_layout.colorImageView
import kotlinx.android.synthetic.main.resource_item_layout.nameTextView
import kotlinx.android.synthetic.main.resource_item_layout.yearTextView
import org.json.JSONObject

class DetailedInfoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_detailed_info)

        init()
    }

    private fun init() {

        goBackButton.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
        }

        val id = intent.extras?.getInt("itemId")

        ApiHandler.getRequest("$UNKNOWN/$id", object : ApiCallback {
            override fun onSuccess(response: String, code: Int) {
                val jsonObject = JSONObject(response)
                val data = jsonObject.getJSONObject("data").toString()
                val model = Gson().fromJson(data, ResourceModel.DataModel::class.java)
                updateUI(model)
            }

            override fun onError(response: String, code: Int) {
                Toast.makeText(this@DetailedInfoActivity, response, Toast.LENGTH_LONG).show()
            }
        })
    }

    private fun updateUI(model: ResourceModel.DataModel) {
        val color = Color.parseColor(model.color)
        colorImageView.setBackgroundColor(color)
        idTextView.text = "ID: ${model.id}"
        nameTextView.text = "Name: ${model.name}"
        yearTextView.text = "Year: ${model.year}"
        colorTextView.text = "Color: ${model.color}"
        pantoneTextView.text = "Pantone value: ${model.pantoneValue}"
    }
}

