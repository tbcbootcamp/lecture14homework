package com.example.listresource

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.resource_item_layout.view.*

class ResourcesAdapter(
    private val items: ArrayList<ResourceModel.DataModel>,
    private val callback: MainActivity
) : RecyclerView.Adapter<ResourcesAdapter.ViewHolder>() {


    interface CustomCallback {
        fun openDetailInfo(id: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.resource_item_layout, parent, false)
        )
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind() {
            val model = items[adapterPosition]
            itemView.colorImageView.setBackgroundColor(Color.parseColor(model.color))
            itemView.nameTextView.text = "Name : ${model.name}"
            itemView.yearTextView.text = "Year : ${model.year}"
            itemView.detailedButton.setOnClickListener {
                callback.openDetailInfo(model.id)
            }
        }
    }
}